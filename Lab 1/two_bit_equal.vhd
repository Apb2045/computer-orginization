library ieee;
use ieee.std_logic_1164.all;

entity two_bit_equal is 
        port (a, b  : in std_logic_vector(1 downto 0);
              aeqb, aNotEqb  : out std_logic);
end two_bit_equal;

architecture arch of two_bit_equal is 
signal p0, p1, p2, p3, p4, p5, p6, p7 : std_logic;
begin
aeqb <= p0 or p1 or p2 or p3;
aNotEqb <= p4 or p5 or p6 or p7;
p0 <= (not a(0)) and (not b(0)) and (not a(1)) and (not b(1));
p1 <= (a(0))     and (b(0))     and (not a(1)) and (not b(1));
p2 <= (not a(0)) and (not b(0)) and (a(1))     and (b(1));
p3 <= (a(0))     and (b(0))     and (a(1))     and (b(1));
p4 <= (not a(0)) and (b(0));
p5 <= (a(0))     and (not b(0));
p6 <= (not a(1)) and (b(1));
p7 <= (a(1))     and (not b(1));
end arch;     