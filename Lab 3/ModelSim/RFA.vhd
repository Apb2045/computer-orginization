library ieee;
use ieee.std_logic_1164.all;

entity RCA is
  port(a, b : in std_logic_vector(3 downto 0);
        cout : out std_logic;
        sum : out std_logic_vector(3 downto 0));
end RCA;

architecture arch of RCA is
  signal c: std_logic_vector(4 downto 0); --"internal" carry tracker
  component FA is
    port(a, b, cin: in std_logic ; --complete at every ?????
          cout : out std_logic;
          sum : out std_logic);
    end component;
  begin
    c(0) <= '0';
    FA0 : FA port map(a(0), b(0), c(0), c(1), sum(0));
    FA1 : FA port map(a(1), b(1), c(1), c(2), sum(1));
    FA2 : FA port map(a(2), b(2), c(2), c(3), sum(2));
    FA3 : FA port map(a(3), b(3), c(3), c(4), sum(3));
    cout <= c(0) or c(1) or c(2) or  c(3);
end arch;