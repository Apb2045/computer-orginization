library IEEE;
use IEEE.std_logic_1164.all;

entity cla4 is -- 4-bit CLA structural model: top entity
  port( a, b: in std_logic_vector(3 downto 0);
      carryin: in std_logic;
      sum: out std_logic_vector(3 downto 0);
      cgout, cpout, overflow: out std_logic);
  end cla4;

architecture arch of cla4 is
  component full_adder_g_p -- component declaration full adder
    port( a, b, cin: in std_logic;
        sum, cg, cp: out std_logic);
    end component;

  component cla_logic -- component declaration CLA-generator
    port( g, p: in std_logic_vector(3 downto 0);
        cin: in std_logic;
        c: out std_logic_vector(2 downto 0);
        cgout, cpout: out std_logic);
  end component;

  signal cg, cp, carry: std_logic_vector(3 downto 0); --local signals
  signal cout: std_logic;
begin
  carry(0) <= carryin;
  ADD0:
      full_adder_g_p
  port map (a(0), b(0), carry(0), sum(0), cg(0), cp(0));
    ADD1:
      full_adder_g_p
  port map (a(1), b(1), carry(1), sum(1), cg(1), cp(1));
    ADD2:
      full_adder_g_p
  port map (a(2), b(2), carry(2), sum(2), cg(2), cp(2));
    ADD3:
      full_adder_g_p
  port map (a(3), b(3), carry(3), sum(3), cg(3), cp(3));
    --generate carries from
    --propagate and generate values
    --from full_adder_g_p_g_p
    CLA:
    cla_logic
  port map(cg, cp, carryin, carry(3 downto 1), cout, cpout);
    cgout <= cout;
    overflow <= carry(3) xor cout;
end arch;
