library ieee;
use ieee.std_logic_1164.all;

entity RCA8 is 
  port(a, b : in std_logic_vector(7 downto 0 );
        cout : out std_logic;
        sum : out std_logic_vector(7 downto 0));
end RCA8;

architecture arch of RCA8 is 

    signal c: std_logic_vector(8 downto 0); --"internal" carry tracker 
    component FA is 
        port(a, b, cin: in std_logic ; --complete at every ?????
             cout : out std_logic ; 
             sum : out std_logic ); 
        end component; 

    begin 
        c(0) <= '0'; 
        FA0 : FA port map(a(0), b(0), c(0), c(1), sum(0)); 
        FA1 : FA port map(a(1), b(1), c(1), c(2), sum(1)); 
        FA2 : FA port map(a(2), b(2), c(2), c(3), sum(2)); 
        FA3 : FA port map(a(3), b(3), c(3), c(4), sum(3));
        FA4 : FA port map(a(4), b(4), c(4), c(5), sum(4)); 
        FA5 : FA port map(a(5), b(5), c(5), c(6), sum(5)); 
        FA6 : FA port map(a(6), b(6), c(6), c(7), sum(6)); 
        FA7 : FA port map(a(7), b(7), c(7), c(8), sum(7));  
        cout <= c(8); 

end arch; 