onerror {quit -f}
vlib work
vlog -work work LPM_Register.vo
vlog -work work LPM_Register.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Ram1Port_vlg_vec_tst
vcd file -direction LPM_Register.msim.vcd
vcd add -internal Ram1Port_vlg_vec_tst/*
vcd add -internal Ram1Port_vlg_vec_tst/i1/*
add wave /*
run -all
