library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity thrity2_four is
port (
	input: in std_logic_vector (31 downto 0);
	out0,out1,out2,out3,out4,out5,out6,out7: out std_logic_vector (3 downto 0));
end thrity2_four;
	
	architecture arch of thrity2_four is 
	begin
		out0 <= input(31 downto 28);
		out1 <= input(27 downto 24);
		out2 <= input(23 downto 20);
		out3 <= input(19 downto 16);
		out4 <= input(15 downto 12);
		out5 <= input(11 downto 8);
		out6 <= input(7 downto 4);
		out7 <= input(3 downto 0);
	end arch;