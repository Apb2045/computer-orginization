library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity eight_by_four is
port (
	data: buffer std_logic_vector(31 downto 0);
	start: in std_logic:='0';
	op: in std_logic_vector(1 downto 0);
	input: in std_logic_vector (7 downto 0));
	end eight_by_four;
	
	architecture arch of eight_by_four is 
	begin
	process (start)
	
	begin 
	if (start='1')
	then 
	case op is
			when "00"=> 
				data (31 downto 24)<= input;
			when "01"=>
				data (23 downto 16)<= input;
			when "10"=>
				data (15 downto 8)<= input;
			when "11"=>
				data (7 downto 0)<= input;
			when others => 
				NULL;
			end case;
		end if;
	end process;
end arch;
