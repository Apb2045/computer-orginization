library ieee;
use ieee.std_logic_1164.all;

entity operations is
port(
        a,b: in std_logic_vector(5 downto 0);
        start: in std_logic;
        op: in std_logic_vector(2 downto 0);
        result: out std_logic_vector(5 downto 0);
		  
		  segment0: out std_logic_vector(6 downto 0);
		  segment1: out std_logic_vector(6 downto 0);
		  segment2: out std_logic_vector(6 downto 0);
		  segmentblank1: out std_logic_vector(6 downto 0);
		  segmentop3:  out std_logic_vector(6 downto 0);
		  segmentop2:  out std_logic_vector(6 downto 0);
		  segmentop1:  out std_logic_vector(6 downto 0);
		  segmentblankop: out std_logic_vector(6 downto 0)
        );
end operations;

architecture arch of operations is
signal in1, in2, out1: std_logic_vector(5 downto 0);

signal Seg_A0,Seg_B0,Seg_C0,Seg_D0,Seg_E0,Seg_F0,Seg_G0: std_logic;
signal Seg_A1,Seg_B1,Seg_C1,Seg_D1,Seg_E1,Seg_F1,Seg_G1: std_logic;
signal opdisplay3,opdisplay2,opdisplay1: std_logic_vector(6 downto 0);

begin
in1 <= a;
in2 <= b;
result <= out1;
segmentop3 <= opdisplay3;
segmentop2 <= opdisplay2;
segmentop1 <= opdisplay1;

process( start )
begin
if( start ='1') then
case op is
when "000" =>
  out1 <= in1 and in2;
  opdisplay3 <= "0001000";
  opdisplay2 <= "1101010";
  opdisplay1 <= "1000010";
when "001" =>
  out1 <= in1 or in2;
  opdisplay3 <= "1100010";
  opdisplay2 <= "0111001";
  opdisplay1 <= "1111111";
when "010" =>
  out1 <= in1 xor in2;
  opdisplay3 <= "1001000";
  opdisplay2 <= "1100010";
  opdisplay1 <= "0111001";
when "011" =>
  out1 <= not in1;
  opdisplay3 <= "1101010";
  opdisplay2 <= "1100010";
  opdisplay1 <= "1001110";
when "100" =>
  out1 <= to_stdlogicvector(to_bitvector(a) sll 1);
  opdisplay3 <= "0100100";
  opdisplay2 <= "1001111";
  opdisplay1 <= "1001111";
when "101" =>
  out1 <= to_stdlogicvector(to_bitvector(a) srl 1);
  opdisplay3 <= "0100100";
  opdisplay2 <= "0111001";
  opdisplay1 <= "1001111";
when "110" =>
  out1 <= to_stdlogicvector(to_bitvector(a) rol 1);
  opdisplay3 <= "0111001";
  opdisplay2 <= "1100010";
  opdisplay1 <= "1001111";
when "111" =>
  out1 <= to_stdlogicvector(to_bitvector(a) ror 1);
  opdisplay3 <= "0111001";
  opdisplay2 <= "1100010";
  opdisplay1 <= "0111001";
when others =>
  --NULL;
  
  
end case;
end if;

Seg_A0 <= ( out1(4) and out1(3) and out1(2) and out1(1) and out1(0)) or ((not out1(4)) and out1(3) and (not out1(2)) and out1(1) and out1(0)) or ( out1(4) and (not out1(3)) and out1(2) and (not out1(1)) and out1(0)) or ((not out1(4)) and (not out1(3)) and (not out1(2)) and (not out1(1)) and out1(0)) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) and out1(1) and (not out1(0))) or (out1(5) and out1(4) and (not out1(3)) and (not out1(2)) and out1(1) and (not out1(0))) or (out1(5) and out1(4) and out1(3) and out1(2) and (not out1(1)) and (not out1(0))) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and out1(2) and (not out1(1)) and (not out1(0))) or ((not out1(5)) and out1(4) and out1(3) and (not out1(2)) and (not out1(1)) and (not out1(0))) or (out1(5) and (not out1(4)) and out1(3) and (not out1(2)) and (not out1(1)) and (not out1(0)));
Seg_B0 <= ( out1(5) and (not out1(4)) and (not out1(3)) and out1(2) and out1(1) ) or (out1(5) and out1(4) and out1(3) and (not out1(2)) and out1(1) ) or ((not out1(4)) and (not out1(3)) and out1(2) and out1(1) and (not out1(0))) or (out1(4) and out1(3) and (not out1(2)) and out1(1) and (not out1(0))) or ( out1(4) and (not out1(3)) and (not out1(2)) and (not out1(1)) and (not out1(0))) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) and out1(1) and out1(0)) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and out1(2) and (not out1(1)) and out1(0)) or ((not out1(5)) and out1(4) and out1(3) and (not out1(2)) and (not out1(1)) and out1(0));
Seg_C0 <= ( out1(5) and out1(4) and out1(3) and out1(2) and out1(1) and (not out1(0))) or ((not out1(5)) and out1(4) and (not out1(3)) and out1(2) and out1(1) and (not out1(0))) or (out1(5) and (not out1(4)) and out1(3) and (not out1(2)) and out1(1) and (not out1(0))) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and (not out1(2)) and out1(1) and (not out1(0))) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) and (not out1(1)) and (not out1(0))) or (out1(5) and out1(4) and (not out1(3)) and out1(2) and (not out1(1)) and (not out1(0))) or (out1(5) and (not out1(4)) and (not out1(3)) and (not out1(2)) and (not out1(1)) and (not out1(0)));
Seg_D0 <= ( (not out1(5)) and (not out1(4)) and (not out1(3)) and out1(2) and out1(1) and out1(0)) or (out1(5) and out1(4) and out1(3) and (not out1(2)) and (not out1(1)) and out1(0)) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) and out1(1) and (not out1(0))) or (out1(5) and out1(4) and (not out1(3)) and (not out1(2)) and out1(1) and (not out1(0))) or (out1(5) and out1(4) and out1(3) and out1(2) and (not out1(1)) and (not out1(0))) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and out1(2) and (not out1(1)) and (not out1(0))) or ((not out1(5)) and out1(4) and out1(3) and (not out1(2)) and (not out1(1)) and (not out1(0))) or (out1(5) and (not out1(4)) and out1(3) and (not out1(2)) and (not out1(1)) and (not out1(0))) or ((not out1(5)) and out1(4) and out1(3) and out1(2) and out1(0)) or (out1(5) and (not out1(4)) and out1(3) and out1(2) and out1(0)) or ((not out1(5)) and out1(4) and (not out1(3)) and (not out1(2)) and out1(0)) or (out1(5) and out1(4) and out1(2) and out1(1) and out1(0)) or (out1(5) and (not out1(4)) and (not out1(2)) and out1(1) and out1(0)) or ((not out1(5)) and out1(3) and (not out1(2)) and out1(1) and out1(0)) or (out1(5) and (not out1(4)) and (not out1(3)) and (not out1(1)) and out1(0)) or ( out1(4) and (not out1(3)) and out1(2) and (not out1(1)) and out1(0)) or ((not out1(4)) and (not out1(3)) and (not out1(2)) and (not out1(1)) and out1(0));
Seg_E0 <= ( (not out1(5)) and out1(0)) or ((not out1(4)) and out1(0)) or (out1(3) and out1(0)) or (out1(2) and out1(0)) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) and out1(1) ) or (out1(5) and out1(4) and out1(3) and out1(2) and (not out1(1)) ) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and out1(2) and (not out1(1)) ) or ((not out1(5)) and out1(4) and out1(3) and (not out1(2)) and (not out1(1)) ) or (out1(5) and (not out1(4)) and out1(3) and (not out1(2)) and (not out1(1)) ) or (out1(5) and out1(4) and (not out1(3)) and (not out1(2)) and (not out1(0))) or (out1(5) and out1(4) and (not out1(3)) and (not out1(2)) and out1(1) );
Seg_F0 <= ( out1(5) and out1(4) and out1(3) and out1(2) and out1(1) ) or ((not out1(5)) and out1(4) and (not out1(3)) and out1(2) and out1(1) ) or (out1(5) and (not out1(4)) and out1(3) and (not out1(2)) and out1(1) ) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and (not out1(2)) and out1(1) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) and (not out1(1)) ) or (out1(5) and out1(4) and (not out1(3)) and out1(2) and (not out1(1)) ) or (out1(5) and (not out1(4)) and (not out1(3)) and (not out1(2)) and (not out1(1)) ) or (out1(5) and out1(4) and (not out1(3)) and (not out1(2)) and out1(1) and out1(0)) or ((not out1(5)) and out1(4) and out1(3) and out1(1) and out1(0)) or (out1(5) and out1(3) and out1(2) and out1(1) and out1(0)) or ((not out1(5)) and (not out1(3)) and out1(2) and out1(1) and out1(0)) or ((not out1(5)) and out1(3) and (not out1(2)) and out1(1) and out1(0)) or (out1(5) and out1(4) and out1(3) and (not out1(1)) and out1(0)) or ((not out1(5)) and out1(4) and (not out1(3)) and (not out1(1)) and out1(0)) or (out1(5) and (not out1(3)) and out1(2) and (not out1(1)) and out1(0)) or (out1(5) and out1(3) and (not out1(2)) and (not out1(1)) and out1(0)) or ((not out1(5)) and (not out1(3)) and (not out1(2)) and (not out1(1)) and out1(0));
Seg_G0 <= ( (not out1(5)) and out1(4) and out1(3) and out1(2) and out1(1) ) or ((not out1(5)) and (not out1(4)) and out1(3) and (not out1(2)) and out1(1) ) or ((not out1(5)) and out1(4) and (not out1(3)) and out1(2) and (not out1(1)) ) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and (not out1(2)) and (not out1(1)) ) or ((not out1(5)) and (not out1(4)) and (not out1(3)) and out1(2) and out1(1) and out1(0)) or (out1(5) and out1(4) and out1(3) and (not out1(2)) and (not out1(1)) and out1(0)) or (out1(5) and out1(4) and (not out1(3)) and out1(2) and out1(1) and (not out1(0))) or (out1(5) and (not out1(4)) and (not out1(3)) and (not out1(2)) and out1(1) and (not out1(0))) or (out1(5) and (not out1(4)) and out1(3) and out1(2) and (not out1(1)) and (not out1(0))) or (out1(5) and (not out1(4)) and out1(3) and out1(1) and out1(0)) or (out1(5) and out1(3) and out1(2) and out1(1) and out1(0)) or ((not out1(5)) and out1(3) and (not out1(2)) and out1(1) and out1(0)) or (out1(5) and (not out1(4)) and (not out1(3)) and (not out1(1)) and out1(0)) or (out1(5) and (not out1(3)) and out1(2) and (not out1(1)) and out1(0)) or ((not out1(5)) and (not out1(3)) and (not out1(2)) and (not out1(1)) and out1(0));
Seg_A1 <= ( out1(4) and (not out1(3)) and (not out1(2)) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(1) ) or ((not out1(4)) and out1(3) and out1(2) and out1(1) ) or (out1(5) and out1(4) and (not out1(3)) and (not out1(1)) ) or ((not out1(4)) and out1(3) and out1(2) and out1(0)) or (out1(5) and out1(4) and (not out1(3)) and (not out1(0)));
Seg_B1 <= '0';
Seg_C1 <= ( (not out1(5)) and out1(4) and (not out1(3)) and out1(2) ) or (out1(5) and (not out1(4)) and (not out1(3)) and out1(2) ) or ((not out1(5)) and out1(4) and out1(3) and (not out1(2)) ) or (out1(5) and (not out1(4)) and out1(3) and (not out1(2)) ) or ((not out1(5)) and out1(4) and out1(2) and (not out1(1)) ) or (out1(5) and (not out1(4)) and (not out1(2)) and out1(1) and out1(0)) or (out1(5) and (not out1(4)) and out1(2) and (not out1(1)) and (not out1(0)));
Seg_D1 <= ( out1(4) and (not out1(3)) and (not out1(2)) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(1) ) or ((not out1(4)) and out1(3) and out1(2) and out1(1) ) or (out1(5) and out1(4) and (not out1(3)) and (not out1(1)) ) or ((not out1(4)) and out1(3) and out1(2) and out1(0)) or (out1(5) and out1(4) and (not out1(3)) and (not out1(0)));
Seg_E1 <= ( out1(4) and (not out1(3)) and (not out1(2)) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(2) ) or ((not out1(5)) and (not out1(4)) and out1(3) and out1(1) ) or ((not out1(5)) and out1(3) and out1(2) and out1(1) ) or ((not out1(4)) and out1(3) and out1(2) and out1(1) ) or (out1(5) and out1(4) and (not out1(3)) and (not out1(1)) ) or (out1(5) and (not out1(3)) and (not out1(2)) and (not out1(1)) ) or ((not out1(4)) and out1(3) and out1(2) and out1(0)) or (out1(5) and out1(4) and (not out1(3)) and (not out1(0))) or (out1(5) and (not out1(3)) and (not out1(2)) and (not out1(0)));
Seg_F1 <= ( (not out1(5)) and out1(4) ) or (out1(5) and (not out1(4)) ) or ((not out1(4)) and out1(3) and out1(2) ) or ( out1(4) and (not out1(3)) and (not out1(2)) ) or ((not out1(4)) and out1(3) and out1(1) ) or ( out1(4) and (not out1(3)) and (not out1(1)) ) or ( out1(4) and (not out1(3)) and (not out1(0)));
Seg_G1 <= ( out1(5) and out1(4) ) or ((not out1(5)) and (not out1(4)) ) or ( out1(4) and (not out1(3)) and (not out1(2)) ) or ((not out1(4)) and out1(3) and out1(2) and out1(1) ) or ((not out1(4)) and out1(3) and out1(2) and out1(0));

segment0(0) <= Seg_A0;
segment0(1) <= Seg_B0;
segment0(2) <= Seg_C0;
segment0(3) <= Seg_D0;
segment0(4) <= Seg_E0;
segment0(5) <= Seg_F0;
segment0(6) <= Seg_G0;

segment1(0) <= Seg_A1;
segment1(1) <= Seg_B1;
segment1(2) <= Seg_C1;
segment1(3) <= Seg_D1;
segment1(4) <= Seg_E1;
segment1(5) <= Seg_F1;
segment1(6) <= Seg_G1;

segment2(0) <= '1';
segment2(1) <= '1';
segment2(2) <= '1';
segment2(3) <= '1';
segment2(4) <= '1';
segment2(5) <= '1';
segment2(6) <= not out1(5);

segmentblank1 <= "1111111";
segmentblankop <= "1111111";

end process;
end arch;
