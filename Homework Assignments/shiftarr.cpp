#include <iostream>

using namespace std;

inline void shift(int*,int);

int main(int argc, char *argv[]) {
	int length = 5;
	int a[length];
	for(int i =0;i<length;i++)
	{
		a[i] = i+1;
	}
	for(int i=0;i<length;i++)
	{
		cout << a[i] << " ";
	}	
	cout << endl;
	shift(a, length);
	for(int i=0;i<length;i++)
	{
		cout << a[i] << " ";
	}	
}

void shift(int*a, int length){
	static int *p;
	for(p=&a[0]; p<&a[length-1];p++)
	{
		*p = *(p+1);
	}
	*p = 0;
}