		.data
myArray:	.word 2:10000			#array to contain 10000 values
size: 		.word 10000			#size of array

		.text
	la   $a0, myArray
	la   $a1, size
	lw   $a1, 0($a1)
	move $t0,$a0	 # p = address of array[0]
	sll $t1,$a1,2 	# $t1 = size * 4
	add $t2,$a0,$t1	 # $t2 = address of array[size]
	loop2: sw $zero,0($t0)	 # Memory[p] = 0
	addi $t0,$t0,4 	# p = p + 4
	slt $t3,$t0,$t2	 # $t3 = (p<&array[size])
	bne $t3,$zero,loop2 	# if (p<&array[size]) go to loop2