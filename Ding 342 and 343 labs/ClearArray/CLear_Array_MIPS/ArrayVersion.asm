
		.data
myArray:	.word 2:10000			#array to contain 10000 values
size: 		.word 10000			#size of array

		.text
	la   $a0, myArray
	la   $a1, size
	lw   $a1, 0($a1)
	move $t0,$zero				# i=0
loop1:	sll $t1,$t0,2			# $t1=i*4
	add $t2,$a0,$t1 			# $t2=address of myArray[1]
	sw $zero, 0($t2)			# myArray[i]=0
	addi $t0,$t0,1				# i= i+1
	slt $t3, $t0, $a1			# $t3=(i<size)
	bne $t3,$zero,loop1			# if(i<size) go to loop1
	jr	$ra

	
