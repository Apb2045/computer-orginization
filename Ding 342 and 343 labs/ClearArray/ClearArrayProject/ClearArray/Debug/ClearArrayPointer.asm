
.686P
.XMM
include listing.inc
.model flat
INCLUDELIB MSVCRTD
INCLUDELIB OLDNAMES
PUBLIC ?clear_array_pointer@@YAXPAHH@Z ; clear_array_pointer
EXTRN __RTC_InitBase:PROC
EXTRN __RTC_Shutdown:PROC
; COMDAT rtc$TMZ
rtc$TMZ SEGMENT
; __RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
rtc$TMZ ENDS
; COMDAT rtc$IMZ
rtc$IMZ SEGMENT
; __RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\test\clear_array_pointer\clear_array_pointer\clear_array_pointer.cpp
; COMDAT ?clear_array_pointer@@YAXPAHH@Z
_TEXT SEGMENT
_p$ = -8 ; size = 4
_ary$ = 8 ; size = 4
_size$ = 12 ; size = 4
?clear_array_pointer@@YAXPAHH@Z PROC ; clear_array_pointer, COMDAT
; 2 : {
push ebp
mov ebp, esp
sub esp, 204 ; 000000ccH
push ebx
push esi
push edi
lea edi, DWORD PTR [ebp-204]
mov ecx, 51 ; 00000033H
mov eax, -858993460 ; ccccccccH
rep stosd
; 3 : int *p;
; 4 : for(p = &ary[0]; p<&ary[size]; p= p+1)
mov eax, DWORD PTR _ary$[ebp]
mov DWORD PTR _p$[ebp], eax
mov ebx, DWORD PTR _size$[ebp]
lea edx, DWORD PTR [eax+ebx*4]
jmp SHORT $LN3@clear_arra
$LN2@clear_arra:
add eax, 4
$LN3@clear_arra:
cmp eax, edx
jae SHORT $LN4@clear_arra
; 5 : *p = 0;
mov DWORD PTR [eax], 0
jmp SHORT $LN2@clear_arra
$LN4@clear_arra:
; 6 : }
pop edi
pop esi
pop ebx
mov esp, ebp
pop ebp
ret 0
?clear_array_pointer@@YAXPAHH@Z ENDP ; clear_array_pointer
_TEXT ENDS
END