to, location
clock, PIN_G26
D[0], PIN_V1
D[1], PIN_V2
data[0], PIN_N25
data[1], PIN_N26
data[2], PIN_P25
data[3], PIN_AE14
data[4], PIN_AF14
data[5], PIN_AD13
data[6], PIN_AC13
data[7], PIN_C13

segment_a, PIN_L3
segment_b, PIN_L2
segment_c, PIN_L9
segment_d, PIN_L6
segment_e, PIN_L7
segment_f, PIN_P9
segment_g, PIN_N9

segment_a2, PIN_R2
segment_b3, PIN_P4
segment_c4, PIN_P3
segment_d5, PIN_M2
segment_e6, PIN_M3
segment_f7, PIN_M5
segment_g8, PIN_M4

segment_a16, PIN_T2
segment_b17, PIN_P6
segment_c18, PIN_P7
segment_d19, PIN_T9
segment_e20, PIN_R5
segment_f21, PIN_R4
segment_g22, PIN_R3

segment_a3, PIN_U9
segment_b4, PIN_U1
segment_c5, PIN_U2
segment_d6, PIN_T4
segment_e7, PIN_R7
segment_f8, PIN_R6
segment_g9, PIN_T3

segment_a30, PIN_AF10
segment_b31, PIN_AB12
segment_c32, PIN_AC12
segment_d33, PIN_AD11
segment_e34, PIN_AE11
segment_f35, PIN_V14
segment_g36, PIN_V13

segment_a4, PIN_V20
segment_b5, PIN_V21
segment_c6, PIN_W21
segment_d7, PIN_Y22
segment_e8, PIN_AA24
segment_f9, PIN_AA23
segment_g10, PIN_AB24

segment_a11, PIN_AB23
segment_b12, PIN_V22
segment_c13, PIN_AC25
segment_d14, PIN_AC26
segment_e15, PIN_AB26
segment_f16, PIN_AB25
segment_g17, PIN_Y24

segment_a18, PIN_Y23
segment_b19, PIN_AA25
segment_c20, PIN_AA26
segment_d21, PIN_Y26
segment_e22, PIN_Y25
segment_f23, PIN_U22
segment_g24, PIN_W24