LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY mod10counter2 IS
PORT (
clock : in std_logic;
reset : in std_logic;
condition: in std_logic_vector(3 downto 0);
clock_out : out std_logic;
output : out std_logic_vector(3 downto 0)
);
END mod10counter2;

ARCHITECTURE Behavior OF mod10counter2 IS
signal counter : integer range 0 to 9 := 0;
BEGIN
PROCESS (reset,clock, condition)
BEGIN
if (reset = '1') then
counter <=0;
elsif rising_edge(clock) then
   if(condition ="0000") then
      if (counter<9) then
      counter<=counter+1;
      clock_out<='0';
      else
      clock_out<='1';
      counter<=0;
      end if;
   elsif(condition="0001") then
      if (counter<9) then
      counter<=counter+1;
      clock_out<='0';
      else
      clock_out<='1';
      counter<=0;
      end if;
   else
      if (counter<3) then
      counter <=counter +1;
      clock_out<='0';
      else
      clock_out<='1';
      counter<=0;
      end if;
end if;
end if;
END PROCESS;
process (counter)
begin
case counter is

when 0 =>
output<="0000";
when 1 =>
output<="0001";
when 2 =>
output<="0010";
when 3 =>
output<="0011";
when 4 =>
output<="0100";
when 5 =>
output<="0101";
when 6 =>
output<="0110";
when 7 =>
output<="0111";
when 8 =>
output<="1000";
when 9 =>
output<="1001";



end case;
end process;


END Behavior;