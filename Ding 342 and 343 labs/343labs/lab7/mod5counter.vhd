LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY mod5counter IS
PORT (
clock : in std_logic;
reset : in std_logic;
clock_out : out std_logic;
output : out std_logic_vector(3 downto 0)
);
END mod5counter;

ARCHITECTURE Behavior OF mod5counter IS
signal counter : integer range 0 to 5 := 0;
BEGIN
PROCESS (reset,clock)
BEGIN
if (reset = '1') then
counter <=0;
elsif rising_edge(clock) then
   if (counter<5) then
   counter<=counter+1;
   clock_out<='0';
   else
   clock_out<='1';
   counter<=0;
   end if;
end if;
END PROCESS;
process (counter)
begin
case counter is

when 0 =>
output<="0000";
when 1 =>
output<="0001";
when 2 =>
output<="0010";
when 3 =>
output<="0011";
when 4 =>
output<="0100";
when 5 =>
output<="0101";




end case;
end process;


END Behavior;