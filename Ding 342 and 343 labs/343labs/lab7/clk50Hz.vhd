library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk50Hz is
    Port (
        clk_in : in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        enable : in  STD_LOGIC;
        clk_out: out STD_LOGIC
    );
end clk50Hz;

architecture Behavioral of clk50Hz is
    signal temporal: STD_LOGIC;
    signal counter : integer range 0 to 249999 := 0;
begin
    frequency_divider: process (reset, clk_in) begin
        if (reset = '1') then
            temporal <= '0';
            counter <= 0;
        elsif (enable ='1') then
            if rising_edge(clk_in) then
               if (counter = 249999) then
               temporal <= NOT(temporal);
               counter <= 0;
               else
               counter <= counter + 1;
               end if;
            else
                temporal <= temporal;
                counter <= counter;
            end if;
        else
               temporal <= temporal;
               counter <= counter;
        end if;
    end process;
    
    clk_out <= temporal;
end Behavioral;