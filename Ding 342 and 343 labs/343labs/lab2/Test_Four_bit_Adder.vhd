Library ieee;
Use ieee.std_logic_1164.all;

Entity Test_Four_bit_Adder is
End Test_Four_bit_Adder; 

Architecture arch of Test_Four_bit_Adder is
component Four_bit_Adder
Port(
	i0,i1 :in std_logic_vector (3 downto 0); 
	Cin :in std_logic;   
	Cout:out std_logic;		
	S:out std_logic_vector(3 downto 0)); 
End component;

signal i0, i1, s :std_logic_vector(3 downto 0);
signal error:std_logic:='0';
signal Cin, Cout: std_logic;

begin
FA:Four_bit_Adder port map(i0=> i0, i1=> i1, Cin=>Cin, Cout=>Cout, S=>S);

process
begin

i0<="0000";
i1<="0000";
Cin<='0';
wait for 1 ns;
if (Cout /='0' or S/="0000") then
	error <= '1';
end if;
wait for 200 ns;

i0<="1010";
i1<="1010";
Cin<='0';
wait for 1 ns;
if (Cout /='1' or S/="0100") then
	error <='1';
end if;
wait for 200 ns;

i0<="1011";
i1<="1010";
Cin<='1';
wait for 1 ns;
if (Cout /='1' or S/="0110") then
	error <='1';
end if;
wait for 200 ns;
	
i0<="0010";
i1<="0010";
Cin<='0';
wait for 1 ns;
if (Cout /='0' or S/="0100") then
	error <='1';
end if;
wait for 200 ns;
	
i0<="0101";
i1<="1010";
Cin<='0';
wait for 1 ns;
if (Cout /='0' or S/="1111") then
	error <='1';
end if;
wait for 200 ns;
	
i0<="0101";
i1<="1010";
Cin<='1';
wait for 1 ns;
if (Cout /='1' or S/="0000") then
	error <='1';
end if;
wait for 200 ns;
	
i0<="0111";
i1<="0111";
Cin<='0';
wait for 1 ns;
if (Cout /='0' or S/="1110") then
	error <='1';
end if;
wait for 200 ns;
	
i0<="1010";
i1<="0111";
Cin<='1';
wait for 1 ns;
if (Cout /='1' or S/="0010") then
	error <='1';
end if;
wait for 200 ns;


if (error = '0') then
	   report "No errors detected. Simulation successful" severity failure;
	else
	    report "Error detected" severity failure;
End if;
End process;
End arch;