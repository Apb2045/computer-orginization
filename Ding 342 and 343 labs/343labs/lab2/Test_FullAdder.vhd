Library ieee;
Use ieee.std_logic_1164.all;

Entity Test_FullAdder is
End Test_FullAdder;


Architecture arch of Test_FullAdder is
component FullAdder
port(
A, B, Cin: in std_logic;
Cout, S: out std_logic);
end component;

signal A, B, Cin, Cout, S:std_logic;
signal error: std_logic:='0';

begin
FA:FullAdder port map (A=>A, B=>B, Cin=>Cin, Cout=>Cout, S=>S);
process
begin
A<='0';
B<='0';
Cin<='0';
wait for 1 ns;
if(Cout='1' or S='1') then
	error <= '1';
end if;
wait for 200 ns;

A<='1';
B<='0';
Cin<='0';
wait for 1 ns;
if(Cout='1' or S='0') then
	error <='1';
end if;
wait for 200 ns;

A<='0';
B<='1';
Cin<='0';
wait for 1 ns;
if(Cout='1' or S = '0') then
	error <='1';
end if;
wait for 200 ns;

A<='1';
B<='1';
Cin<='0';
wait for 1 ns;
if(Cout='0' or S='1') then
	error <='1';
end if;
wait for 200 ns;

A<='0';
B<='0';
Cin<='1';
wait for 1 ns;
if(Cout='1' or S='0') then
	error <='1';
end if;
wait for 200 ns;

A<='1';
B<='0';
Cin<='1';
wait for 1 ns;
if(Cout='0' or S='1') then
	error <='1';
end if;
wait for 200 ns;

A<='0';
B<='1';
Cin<='1';
wait for 1 ns;
if(Cout='0' or S='1') then
	error <='1';
end if;
wait for 200 ns;

A<='1';
B<='1';
Cin<='1';
wait for 1 ns;
if(Cout='0' or S='0') then
	error <='1';
end if;
wait for 200 ns;

if (error = '0') then
	   report "No errors detected. Simulation successful" severity failure;
	else
	   report "Error detected" severity failure;
end if;
	   
end process;
End arch;
