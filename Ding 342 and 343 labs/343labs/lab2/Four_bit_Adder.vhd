Library ieee;
Use ieee.std_logic_1164.all;

Entity Four_bit_Adder is
Port(
	i0,i1 :in std_logic_vector (3 downto 0); 
	Cin :in std_logic;   
	Cout:out std_logic;		
	S:out std_logic_vector(3 downto 0)); 
End Four_bit_Adder;

Architecture arch of Four_bit_Adder is
component FullAdder
Port(
	A, B, Cin :in std_logic;
	Cout, S:out std_logic);
End component;

signal temp: std_logic_vector(2 downto 0);
begin
FA1: FullAdder port map (A=>i0(0), B=>i1(0), Cin=>Cin, Cout=>temp(0), S=>S(0));
FA2: FullAdder port map (A=>i0(1), B=>i1(1), Cin=>temp(0), Cout=>temp(1), S=>S(1));
FA3: FullAdder port map (A=>i0(2), B=>i1(2), Cin=>temp(1), Cout=>temp(2), S=>S(2));
FA4: FullAdder port map (A=>i0(3), B=>i1(3), Cin=>temp(2), S=>S(3),Cout=>Cout);

end arch;



