LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY Part_2 IS
  PORT (
  reset, clock: in std_logic;
  w:in std_logic_vector(1 downto 0);
  counter:out std_logic_vector(3 downto 0)
);
END Part_2;
ARCHITECTURE Behavior OF Part_2 IS
  TYPE State_type IS (c0,c1,c2,c3,c4,c5,c6,c7,c8,c9);
  SIGNAL present_state, next_state : State_type; -- y_Q is present state, y_D is next state
BEGIN
 
  PROCESS (w, present_state) -- state table
  BEGIN
       case present_state IS     
		 WHEN c0 => if(w="00")Then next_state<=c0;
						elsif w="01" then
							next_state<=c1;
						elsif w="10" then
							next_state<=c2;
						elsif w="11" then
							next_state<=c9;
						end if;
						
						
						
        WHEN c1 =>  if(w="00")Then next_state<=c1;
						elsif w="01" then
							next_state<=c2;
						elsif w="10" then
							next_state<=c3;
						elsif w="11" then
							next_state<=c0;
						end if;
						
						
        WHEN c2 =>  if(w="00")Then next_state<=c2;
						elsif w="01" then
							next_state<=c3;
						elsif w="10" then
							next_state<=c4;
						elsif w="11" then
							next_state<=c1;
						end if;
						
						
		
		 WHEN c3 => if(w="00")Then next_state<=c3;
						elsif w="01" then
							next_state<=c4;
						elsif w="10" then
							next_state<=c5;
						elsif w="11" then
							next_state<=c2;
						end if;
						
			
		WHEN c4 =>  if(w="00")Then next_state<=c4;
						elsif w="01" then
							next_state<=c5;
						elsif w="10" then
							next_state<=c6;
						elsif w="11" then
							next_state<=c3;
						end if;
						
						
        WHEN c5 =>  if(w="00")Then next_state<=c5;
						elsif w="01" then
							next_state<=c6;
						elsif w="10" then
							next_state<=c7;
						elsif w="11" then
							next_state<=c4;
						end if;
						
						
		
    	WHEN c6 => 	if(w="00")Then next_state<=c6;
						elsif w="01" then
							next_state<=c7;
						elsif w="10" then
							next_state<=c8;
						elsif w="11" then
							next_state<=c5;
						end if;
						
						
			
        WHEN c7 =>  if(w="00")Then next_state<=c7;
						elsif w="01" then
							next_state<=c8;
						elsif w="10" then
							next_state<=c9;
						elsif w="11" then
							next_state<=c6;
						end if;
						
			
        WHEN c8 =>  if(w="00")Then next_state<=c8;
						elsif w="01" then
							next_state<=c9;
						elsif w="10" then
							next_state<=c0;
						elsif w="11" then
							next_state<=c7;
						end if;
						
		
		when c9 => if(w="00")Then next_state<=c9;
						elsif w="01" then
							next_state<=c0;
						elsif w="10" then
							next_state<=c1;
						elsif w="11" then
							next_state<=c8;
						end if;
						
	
       END CASE;
  END PROCESS; 

  PROCESS (Clock) 
  begin
	if reset='1' then
		if (rising_edge(clock)) then
			present_state<=next_state;
		end if;
	else 
		present_state<=c0;
	end if;
  	
  END PROCESS;
  process (present_state)
  begin
	case present_state is 
	when c0=> counter<="0000";
	when c1=> counter<="0001";
	when c2=> counter<="0010";
	when c3=> counter<="0011";
	when c4=> counter<="0100";
	when c5=> counter<="0101";
	when c6=> counter<="0110";
	when c7=> counter<="0111";
	when c8=> counter<="1000";
	when c9=> counter<="1001";		
	end case;
  end process;
END Behavior;
