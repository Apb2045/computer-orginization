LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY Part_I IS
  PORT (
  w, clock, reset:IN std_logic;
	z: OUT std_logic;
	LED:out std_logic_vector(8 downto 0)
);
END Part_I;
ARCHITECTURE Behavior OF Part_I IS
  TYPE State_type IS (start, B, C, D, E, F, G, H, I);
  SIGNAL present_state, next_state : State_type; -- y_Q is present state, y_D is next state
BEGIN
 
  PROCESS (w, present_state) -- state table
  BEGIN
       case present_state IS     
		WHEN start => if(w='0')Then
							next_state<=B;
						else 
							next_state<=F;
						end if;
						LED<="000000001";
						
		
        WHEN B =>  if(w='0') then
							next_state<=C;
							else
								next_state<=F;
					end if;
			 
						LED<="000000010";
			
        WHEN C =>  if(w='0') then
							next_state<=D;
							else
								next_state<=F;
					end if;
			 
						LED<="000000100";
		
		 WHEN D => if(w='0') then
							next_state<=E;
							else
								next_state<=F;
					end if;
			 
						LED<="000001000";
			
             WHEN E =>  if(w='0') then
							next_state<=E;
							else
								next_state<=F;
						end if;
			 
						LED<="000010000";
			
             WHEN F =>  if(w='0') then
							next_state<=B;
							else
								next_state<=G;
						end if;
			 
						LED<="000100000";
		
    		 WHEN G => if(w='0') then
							next_state<=B;
							else
								next_state<=H;
						end if;
			 
						LED<="001000000";
			
             WHEN H =>  if(w='0') then
							next_state<=B;
							else
								next_state<=I;
						end if;
			 
						LED<="010000000";
			
             WHEN I =>  if(w='0') then
							next_state<=B;
							else
								next_state<=I;
						end if;
			 
						LED<="100000000";
		
	
       END CASE;
  END PROCESS; 

  PROCESS (Clock) 
  begin
	if reset='1' then
		if (rising_edge(clock)) then
			present_state<=next_state;
		end if;
	else 
		present_state<=start;
	end if;
  	
  END PROCESS;
  
  process (present_state)
  begin
	case present_state is 
	when E=>
			Z<='1';
	when I=>
			Z<='1';
	when others =>
			Z<='0';
	end case;
  
  end process;
  
END Behavior;
