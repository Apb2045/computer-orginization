LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY Count IS
	PORT
	(
		Clk			: in std_logic ;
		Clear		: in std_logic ;
		Q			: out std_logic_vector (1 downto 0)
	);
END Count;

Architecture arch of Count is
	signal count : std_logic_vector(1 downto 0);
begin
	process(clk)
	begin
		if (rising_edge(clk)) then
			if clear = '1' then
				count <= "00";
			else 
				count <= count + 1;
			end if;
		end if;
	end process;
	Q <= count;
end arch;
