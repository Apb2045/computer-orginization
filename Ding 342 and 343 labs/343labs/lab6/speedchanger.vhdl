library ieee;
use ieee.std_logic_1164.all;
entity speedchanger is
port(
w, clock:in std_logic;
cout:out std_logic_vector(2 downto 0));
end speedchanger;

architecture behavior of speedchanger is
type state_type is (s0, s1, s2, s3, s4);
signal present_state, next_state:state_type;

begin
process (w, present_state)
begin
	case present_state is
		when s0=> if(w='1') then next_state<=s1;
		else next_state<=s0;
		end if;
		
		when s1=> if(w='1') then next_state<=s2;
		else next_state<=s0;
		end if;
		
		when s2=> if(w='1') then next_state<=s3;
		else next_state<=s1;
		end if;
		
		when s3=> if(w='1') then next_state<=s4;
		else next_state<=s2;
		end if;
		
		when s4=> if(w='1') then next_state<=s4;
		else next_state<=s3;
		end if;
		
	end case;
	end process;
	
	process(present_state)
	begin
		case present_state is
			when s0=> cout<="000";
			when s1=> cout<="001";
			when s2=> cout<="010";
			when s3=> cout<="011";
			when s4=> cout<="100";
		end case;
	end process;
	end behavior;
	
		