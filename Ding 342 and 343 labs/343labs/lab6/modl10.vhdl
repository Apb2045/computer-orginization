LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY modl10 IS
  PORT (
  reset, clock,clk_en: in std_logic;
  cout:out std_logic;
  counter:out std_logic_vector(3 downto 0)
);
END modl10;
ARCHITECTURE Behavior OF modl10 IS
  TYPE State_type IS (c0,c1,c2,c3,c4,c5,c6,c7,c8,c9);
  SIGNAL present_state, next_state : State_type; -- y_Q is present state, y_D is next state
BEGIN
 
  PROCESS (clock, present_state) -- state table
  BEGIN
       case present_state IS     
		 WHEN c0 => if(rising_edge(clock))Then next_state<=c1;
						else
							next_state<=c0;
						end if;
						
						
						
        WHEN c1 =>  if(rising_edge(clock))Then next_state<=c2;
						else
							next_state<=c1;
						end if;
						
						
        WHEN c2 =>  if(rising_edge(clock))Then next_state<=c3;
						else
							next_state<=c2;
						end if;
						
						
		
		 WHEN c3 => if(rising_edge(clock))Then next_state<=c4;
						else
							next_state<=c3;
						end if;
						
			
		WHEN c4 =>  if(rising_edge(clock))Then next_state<=c5;
						else
							next_state<=c4;
						end if;
						
						
        WHEN c5 =>  if(rising_edge(clock))Then next_state<=c6;
						else
							next_state<=c5;
						end if;
						
						
		
    	WHEN c6 => 	if(rising_edge(clock))Then next_state<=c7;
						else
							next_state<=c6;
						end if;
						
						
			
        WHEN c7 =>  if(rising_edge(clock))Then next_state<=c8;
						else
							next_state<=c7;
						end if;
						
			
        WHEN c8 =>  if(rising_edge(clock))Then next_state<=c9;
						else
							next_state<=c8;
						end if;
						
		
		when c9 =>  if(rising_edge(clock))Then next_state<=c0;
						cout<='1';
						else
							next_state<=c9;
						end if;
						
	
       END CASE;
  END PROCESS; 

  PROCESS (Clock) 
  begin
	if reset='1' then		
		present_state<=next_state;
	else 
		present_state<=c0;
	end if;
  	
  END PROCESS;
  process (present_state)
  begin
	case present_state is 
	when c0=> counter<="0000";
	when c1=> counter<="0001";
	when c2=> counter<="0010";
	when c3=> counter<="0011";
	when c4=> counter<="0100";
	when c5=> counter<="0101";
	when c6=> counter<="0110";
	when c7=> counter<="0111";
	when c8=> counter<="1000";
	when c9=> counter<="1001";		
	end case;
  end process;
END Behavior;
