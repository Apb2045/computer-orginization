to, location
clock, PIN_N2
clk_en, PIN_V2
aclr, PIN_P23
w, PIN_V1
ck, PIN_N23
segment_a, PIN_AF10
segment_b, PIN_AB12
segment_c, PIN_AC12
segment_d, PIN_AD11
segment_e, PIN_AE11
segment_f, PIN_V14
segment_g, PIN_V13

segment_a1, PIN_V20
segment_b1, PIN_V21
segment_c1, PIN_W21
segment_d1, PIN_Y22
segment_e1, PIN_AA24
segment_f1, PIN_AA23
segment_g1, PIN_AB24

segment_a2, PIN_AB23
segment_b2, PIN_V22
segment_c2, PIN_AC25
segment_d2, PIN_AC26
segment_e2, PIN_AB26
segment_f2, PIN_AB25
segment_g2, PIN_Y24

segment_a3, PIN_Y23
segment_b3, PIN_AA25
segment_c3, PIN_AA26
segment_d3, PIN_Y26
segment_e3, PIN_Y25
segment_f3, PIN_U22
segment_g3, PIN_W24

segment_a4, PIN_U9
segment_b4, PIN_U1
segment_c4, PIN_U2
segment_d4, PIN_T4
segment_e4, PIN_R7
segment_f4, PIN_R6
segment_g4, PIN_T3

segment_a5, PIN_T2
segment_b5, PIN_P6
segment_c5, PIN_P7
segment_d5, PIN_T9
segment_e5, PIN_R5
segment_f5, PIN_R4
segment_g5, PIN_R3

segment_a6, PIN_R2
segment_b6, PIN_P4
segment_c6, PIN_P3
segment_d6, PIN_M2
segment_e6, PIN_M3
segment_f6, PIN_M5
segment_g6, PIN_M4

segment_a7, PIN_L3
segment_b7, PIN_L2
segment_c7, PIN_L9
segment_d7, PIN_L6
segment_e7, PIN_L7
segment_f7, PIN_P9
segment_g7, PIN_N9