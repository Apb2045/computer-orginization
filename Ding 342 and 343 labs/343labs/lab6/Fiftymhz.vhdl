library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Fiftymhz is
    Port (
        clk_in,clk_en : in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        clk_out: out STD_LOGIC
    );
end Fiftymhz;

architecture Behavioral of Fiftymhz is
    signal temporal: STD_LOGIC;
    signal counter : integer range 0 to 1 := 0;
begin
    frequency_divider: process (reset, clk_in) begin
        if (reset = '1') then
            temporal <= '0';
            counter <= 0;
        elsif clk_en='1' then
        if(rising_edge(clk_in)) then
            if (counter = 1) then
                temporal <= NOT(temporal);
                counter <= 0;
            else
                counter <= counter + 1;
            end if;
            end if;
        end if;
    end process;
    
    clk_out <= temporal;
end Behavioral;