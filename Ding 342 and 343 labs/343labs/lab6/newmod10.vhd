LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY newmod10 is
port(
clock,aclr:in std_logic;
output:out std_logic_vector(3 downto 0);
cout:out std_logic);
end newmod10;

architecture arch of newmod10 is

signal counter:integer range 0 to 9:=0;
begin 
process (aclr,clock)
begin
if(aclr='1')then
counter<=0;
elsif(rising_edge(clock)) then
   if (counter<9) then
  counter<=counter+1;
  cout<='0';
  else
  cout<='1';
  counter<=0;
 end if;
 end if;
end process;
process(counter)
begin
case counter is
when 0=>output<="0000";
when 1=>output<="0001";
when 2=>output<="0010";
when 3=>output<="0011";
when 4=>output<="0100";
when 5=>output<="0101";
when 6=>output<="0110";
when 7=>output<="0111";
when 8=>output<="1000";
when 9=>output<="1001";
 end case;
 end process;
 end arch;